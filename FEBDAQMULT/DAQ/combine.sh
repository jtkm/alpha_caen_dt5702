#!/bin/bash
#First argument should be the folder where the Voltage_XXX.csv files are in
SUBFOLDER=${1}
#Save current path for later
OLDPATH=`pwd`
#Go to 'working folder'
cd ${SUBFOLDER}

rm -f Total.csv
rm -f temp.csv
rm -f column.csv
#Put row titles in temp.csv
cat `ls -tr Voltage_*csv | head -n 1` | awk -F ',' '{ print $1 }' > temp.csv
for i in `ls -tr Voltage_*csv `; do 
   #Put column labels directly into Total.csv
   echo -n ",${i}" >> Total.csv
   #Get a column of data, store it in column.csv (over write every time)
   cat ${i} | awk -F ',' '{ print $2 }' > column.csv
   #Horizontally concatinate the temp.csv with the new column
   paste -d ',' temp.csv column.csv > temp2.csv
   #Replace temp.csv with the newer version
   mv -f temp2.csv temp.csv
done
#Add a new line at the end of the titles
echo "," >> Total.csv
#Add the data we just compiled to the filer with the headers
cat temp.csv >> Total.csv

#Clean up
rm -f temp.csv
rm -f column.csv
#Go back to original path
cd ${OLDPATH}
echo "Done in ${SUBFOLDER}"
