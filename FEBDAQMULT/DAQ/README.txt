

How to use:

1. Become root:
$ su

2. Load the GUI:
$ root -l 'FEBDAQMULT.C+("enp3s0")'

3. Set your gain manually in the GUI

4a. Scan settings... look at the top of FEBDAQMULT.C for the range 
and step size... sorry this is hard coded... edit as you need, quit 
root (.q) and go back to step 1.
4b. Run you scan (when 0 is the number for your ADC channel)
$ ScanThresAndOverVoltage(0)
4c. Give a folder name where you want this scan to be saved

5. Quit root (.q) and combine the data you just took:
. combine.sh folder_name

6. Copy Total.csv to where you want to keep it... 
