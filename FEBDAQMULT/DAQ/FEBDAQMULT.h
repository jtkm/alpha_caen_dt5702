#include "TBenchmark.h"
#include "TGraph.h"
#include "TH1F.h"
#include "TH2F.h"

#include "FEBDTP.hxx"
#include "TSystem.h"

// Mainframe macro generated from application: /home/home/root-6.02.02/bin/root.exe
// By ROOT version 6.02/02 on 2015-05-17 23:03:49

#ifndef ROOT_TGDockableFrame
#include "TGDockableFrame.h"
#endif
#ifndef ROOT_TGMenu
#include "TGMenu.h"
#endif
#ifndef ROOT_TGMdiDecorFrame
#include "TGMdiDecorFrame.h"
#endif
#ifndef ROOT_TG3DLine
#include "TG3DLine.h"
#endif
#ifndef ROOT_TGMdiFrame
#include "TGMdiFrame.h"
#endif
#ifndef ROOT_TGMdiMainFrame
#include "TGMdiMainFrame.h"
#endif
#ifndef ROOT_TGMdiMenu
#include "TGMdiMenu.h"
#endif
#ifndef ROOT_TGListBox
#include "TGListBox.h"
#endif
#ifndef ROOT_TGNumberEntry
#include "TGNumberEntry.h"
#endif
#ifndef ROOT_TGScrollBar
#include "TGScrollBar.h"
#endif
#ifndef ROOT_TGComboBox
#include "TGComboBox.h"
#endif
#ifndef ROOT_TGuiBldHintsEditor
#include "TGuiBldHintsEditor.h"
#endif
#ifndef ROOT_TGuiBldNameFrame
#include "TGuiBldNameFrame.h"
#endif
#ifndef ROOT_TGFrame
#include "TGFrame.h"
#endif
#ifndef ROOT_TGFileDialog
#include "TGFileDialog.h"
#endif
#ifndef ROOT_TGShutter
#include "TGShutter.h"
#endif
#ifndef ROOT_TGButtonGroup
#include "TGButtonGroup.h"
#endif
#ifndef ROOT_TGCanvas
#include "TGCanvas.h"
#endif
#ifndef ROOT_TGFSContainer
#include "TGFSContainer.h"
#endif
#ifndef ROOT_TGuiBldEditor
#include "TGuiBldEditor.h"
#endif
#ifndef ROOT_TGColorSelect
#include "TGColorSelect.h"
#endif
#ifndef ROOT_TGButton
#include "TGButton.h"
#endif
#ifndef ROOT_TGFSComboBox
#include "TGFSComboBox.h"
#endif
#ifndef ROOT_TGLabel
#include "TGLabel.h"
#endif
#ifndef ROOT_TGMsgBox
#include "TGMsgBox.h"
#endif
#ifndef ROOT_TRootGuiBuilder
#include "TRootGuiBuilder.h"
#endif
#ifndef ROOT_TGTab
#include "TGTab.h"
#endif
#ifndef ROOT_TGListView
#include "TGListView.h"
#endif
#ifndef ROOT_TGSplitter
#include "TGSplitter.h"
#endif
#ifndef ROOT_TGStatusBar
#include "TGStatusBar.h"
#endif
#ifndef ROOT_TGListTree
#include "TGListTree.h"
#endif
#ifndef ROOT_TGuiBldGeometryFrame
#include "TGuiBldGeometryFrame.h"
#endif
#ifndef ROOT_TGToolTip
#include "TGToolTip.h"
#endif
#ifndef ROOT_TGToolBar
#include "TGToolBar.h"
#endif
#ifndef ROOT_TRootEmbeddedCanvas
#include "TRootEmbeddedCanvas.h"
#endif
#ifndef ROOT_TCanvas
#include "TCanvas.h"
#endif
#ifndef ROOT_TGuiBldDragManager
#include "TGuiBldDragManager.h"
#endif

#include "Riostream.h"
#include "TMath.h"
#include "TF1.h"
#include "TTree.h"
#include "time.h"
#include <sys/timeb.h>
#include "Dialogs.C"

#define maxpe 10 //max number of photoelectrons to use in the fit
int NEVDISP=200; //number of lines in the waterfall event display
const Double_t initpar0[7]={7000,100,700,9.6,1.18,0.3,0.5};
const Double_t initpar1[7]={3470,100,700,9.5,2.25,3e-3,3.7e-2};
Double_t peaks[maxpe]; //positions of peaks in ADC counts
Double_t peaksint[maxpe]; //expected integral of each p.e. peak
UShort_t VCXO_Value=500; //DAC settings to correct onboard VCTCXO
UShort_t VCXO_Values[256]; //DAC settings to correct onboard VCTCXO per board, index=mac5
TGNumberEntry *fNumberEntry75;
TGNumberEntry *fNumberEntry755;
TGNumberEntry *fNumberEntry886;
TGNumberEntry *fNumberEntry8869;
TGNumberEntry *fNumberEntry90;
TGNumberEntry *fNumberEntry91;
TGNumberEntry *fNumberEntry92;
TGNumberEntry *fNumberEntryCalibrateNo;
TGNumberEntry *fNumberEntryTME;
//TGLabel *fLabel;
TGStatusBar *fStatusBar739;
   TGRadioButton * fChanProbe[33];
   TGCheckButton * fChanEnaAmp[34];
   TGCheckButton * fChanEnaTrig[33];
   TGNumberEntry * fChanGain[32];
   TGNumberEntry * fChanBias[32];
   TGCheckButton *fUpdateHisto;
   TGCheckButton *fUpdateVCXO;
   TGTab *fTab683;
   TGLabel *fLabel7;
   TBenchmark *BenchMark; 
TF1* f0;
TF1* f1;
UChar_t mac5=0x00;
UChar_t bufPMR[1500];
UChar_t bufSCR[1500];
UChar_t buf[1500];
TH1F * hst[32];
TCanvas *c=0;
TCanvas *c1=0;
TCanvas *c3=0;
TCanvas *c4=0;
TCanvas *c5=0;
TCanvas *c6=0;
TGraph *grevrate=0;
TGraph *gr=0;
TGraph *gts0[256];
TGraph *gts1[256];
TH1F *hcprof=0;
TH2F *hevdisp=0;
void FillHistos(int truncat);
  int evs=0; //overall events per DAQ
 // int evs_notfirst=0; //overall events per DAQ without very first request
  int evsperrequest=0;
FEBDTP* t;
Int_t chan=0; //channel to display on separate canvas c1
Int_t BoardToMon=0; //board to display on separate canvas c1
TTree * tr;
int RunOn=0;

time_t tm0,tm1;

uint32_t CHAN_MASK=0xFFFFFFFF; // by default all channels are recorded

void FEBGUI();
void SetThresholdDAC1(UShort_t dac1);
UShort_t GetThresholdDAC1();
void SetThresholdDAC2(UShort_t dac2);
int Init(const char* iface);
float GetTriggerRate();
void UpdateConfig();
void UpdateHisto();
void UpdateBoardMonitor();
void DAQ(int nev);
void RescanNet();
UChar_t ConfigGetGain(int chan);
UChar_t ConfigGetBias(int chan);
void ConfigSetGain(int chan, UChar_t val);
void ConfigSetBias(int chan, UChar_t val);
void ConfigSetFIL(uint32_t mask1, uint32_t mask2, uint8_t majority); 
 
 
void ThresholdScan(UShort_t step,UShort_t start, UShort_t stop, int nmeasurements=1, int sleep_time=1000, const char* filename=NULL);


void WriteConfig(const char* path, UChar_t *buffer=bufSCR, UShort_t bitlen=1144);
